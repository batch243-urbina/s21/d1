// console.log(`Happy Monday!`);

// Array - list of data that are related with each other
// Array -special kind of object

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

let studentNumbers = [
  "2020-1923",
  "2020-1924",
  "2020-1925",
  "2020-1926",
  "2020-1927",
];

// Arrays are used to store multiple related values
// Array literals []
// Array also provide access a number of function/methods that help achieving a task
// Method - functions associated with an object/array
// Method - used to manipulate information stored within the same object

let grades = [98.5, 94.3, 89.2, 90.1];
console.log(grades);

let computerBrands = ["Acer", "Asus", "Dell", "Lenovo", "Mac", "Samsung"];
console.log(computerBrands);

let mixedArr = [12, "Asus", null, undefined, []];
console.log(mixedArr);

let myTasks = ["drink HTML", "eat javascript", "bake sass", "inhale css"];
console.log(myTasks);

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "New York";

let cities = [city1, city2, city3];
console.log(cities);

// .length property - allows us to get the total number of items in an array
console.log(grades.length);

let emptyArray = [];
console.log(emptyArray);
console.log(emptyArray.length);

// .length property can also be used with strings

let nameJohn = "John Doe";
console.log(nameJohn.length);
console.log(myTasks[1].length);

// .length property can also set the total number of items in an array, we can delete the last item in array

myTasks.length = myTasks.length - 1;
console.log(myTasks);

cities.length--;
console.log(cities);

// Lengthen it by adding a number into length property

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length = theBeatles.length + 1;
theBeatles[4] = "Edward";
console.log(theBeatles);

// reading/accession elements of arrays
// accessing array elements more common task that we do with an array
// use of index

let lakersLegend = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];

console.log(lakersLegend[1]);
console.log(lakersLegend[4]);

// save/store array elements in another variable
let currentLaker = lakersLegend[2];

console.log(currentLaker);

// can also reassign array values using indices
console.log(`Array before the reassignment: `);
console.log(lakersLegend);

lakersLegend[2] = "Pau";
console.log(`Array after the reassignment`);
console.log(lakersLegend);

// accessing the last element of a array
// length - 1 is last element

let lastElementIndex = lakersLegend.length - 1;
console.log(lakersLegend[lastElementIndex]);

// adding items without using array methods
let newArr = [];
newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[newArr.length] = "Tifa Lockhard";
console.log(newArr);

newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

// looping over an array
// use a for loop to iterate overall items in an array

for (let index = 0; index < newArr.length; index++) {
  console.log(newArr[index]);
}

// loop to check if elements are divisible by 5

let numArr = [5, 12, 30, 46, 40];

for (let index = 0; index < numArr.length; index++) {
  if (numArr[index] % 5 === 0) {
    console.log(`The number ${numArr[index]} is divisible by 5`);
  } else {
    console.log(
      `The number ${numArr[index]} is not divisible by 5! Next number!`
    );
  }
}

// Multidimensional arrays
// strong complex data structures
// help visualize/create real world objects

let chessBoard = [
  ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
  ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
  ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
  ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
  ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
  ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
  ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
  ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];

console.table(chessBoard);

console.log(chessBoard[3][5]);
